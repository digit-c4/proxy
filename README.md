# proxy


## Getting started

This project deploys proxy settings and configuration to the infrastructure

## to be improved

## Testing
Prepare the test suite environment
```bash
python3 -m venv ./venv && source ./venv/bin/activate
poetry install --only test
```

### Static tests
```bash
# Check yaml linting
poetry run yamllint .
# Check py linting in modules directory
poetry run flake8 ./modules
```
